# BalaBox : Service d'enregistrement et de tansfert audio, photo et vidéo

Ce dépôt contient le code source du service d'enregistrement et de
tansfert audio, photo et vidéo intégré à la [BalaBox], ainsi que la
documentation y afférente.

## Le service d'enregistrement et de tansfert audio, photo et vidéo

Le service d'enregistrement se présente sous la forme d'une
application Web qui offre à l'élève une fonction de prise et
d'enregistement de sons (dictaphone), de photos et de vidéos. Pour ce
faire, cette application exploite le matériel des terminaux mobiles
des élèves (i.e. micro, caméra). Il permet à l'élève de transférer sa
production à l'enseignant à travers cette application Web. Il pourra
également à travers cette application Web, modifier et supprimer sa
production avant transfert.

Ce service permet également à l'enseignant d'obtenir les productions
des élèves, de les sauvegarder et de les supprimer. L'enseignant n'a
pas à raffraichir la page listant les productions des élèves. Elle est
mise à jour automatiquement lorsqu'un élève transfert sa production.


## Liste des fonctionnalités du service
* Prise de photos, vidéos ou sons
* Soumission de la production de l'élève
* Nommage automatique de la production lors de la soumission avec le
  nom de l'élève, ou le groupe d'élèves (l'identifiant de l'élève peut
  être obtenu par le service d'identification).
* Réécoute de la production avant soumission et validation définitive.
* Possibilité pour l’élève de modifier/supprimer recommencer sa
  production avant soumission.
* Étiquetage des productions des élèves par l'enseignant. 
* Suivi (en direct) des soumissions par l'enseignant.
* Consultation (i.e. écoute, visualisation) des productions.
* Sauvegarde des productions sur un support externe (e.g. clé USB).
 
## Mise en œuvre du service
Le service est composé de 2 interfaces: une interface élève et une
interface enseignant.

Le service exploite le service d'identification de la [BalaBox]pour
identifier l'élève.

Pour assurer un partage en direct des productions, les interfaces
utilisateur des élèves et de l'enseignant reposent sur des WebSockets.

[balabox]: https://balabox.gitlab.io/balabox/
[moodlebox]: https://moodlebox.net


